#include <Music.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include <gccore.h>
#include <oggplayer.h>
#include <asndlib.h>

Music::Music() :
    mBuffer(nullptr),
    mBufferLen(0)
{
}

Music::~Music()
{
}

bool Music::Init(const void* buffer, int len)
{
    mBuffer = (void*)buffer;
    mBufferLen = len;
    return true;
}

void Music::Play(bool loop)
{
    int ret = PlayOgg(
        mBuffer,
        mBufferLen,
        0,
        loop ? OGG_INFINITE_TIME : OGG_ONE_TIME
    );
    (void)ret;
}

void Music::Stop()
{
    StopOgg();
}

void Music::Pause()
{
    // 1 == pause
    PauseOgg(1);
}

void Music::Resume()
{
    // 0 == resume
    PauseOgg(0);
}