#include <VertexBuffer.h>
#include <string.h>

VertexBuffer::VertexBuffer() :
    mVertices(nullptr),
    mVerticesSize(0),
    mIndices(nullptr),
    mIndicesSize(0),
    mFloatsPerVertex(0)
{
}

VertexBuffer::~VertexBuffer()
{
    if (mVertices != nullptr) delete[] mVertices;
    if (mIndices != nullptr) delete[] mIndices;
}

void VertexBuffer::Init(
    float* vertices,
    size_t verticesSize,
    int* indices,
    size_t indicesSize,
    size_t floatsPerVertex)
{
    mVertices = new float[verticesSize];
    mIndices = new int[indicesSize];
    mVerticesSize = verticesSize;
    mIndicesSize = indicesSize;
    mFloatsPerVertex = floatsPerVertex;
    
    memcpy((void*)mVertices, (void*)vertices, verticesSize*sizeof(float));
    if (indicesSize > 0) {
        memcpy((void*)mIndices, (void*)indices, indicesSize*sizeof(int));
    }
}

void VertexBuffer::UpdateVertices(
    float* vertices,
    size_t verticesSize/*,
    int* indices,
    size_t indicesSize,
    size_t floatsPerVertex*/)
{
    
    /*if (verticesSize != mVerticesSize) {
        if (mVertices) {
            delete[] mVertices;
            mVertices = nullptr;
        }
        mVertices = new float[verticesSize];
        mVerticesSize = verticesSize;
    }*/
    memcpy((void*)mVertices, (void*)vertices, verticesSize*sizeof(float));
    
    /*if (indicesSize != mIndicesSize && mIndicesSize > 0) {
        delete[] mIndices;
        mIndices = nullptr;
        mIndicesSize = indicesSize;
        if (indicesSize > 0) {
            mIndices = new int[indicesSize];
        }
    }
    if (indicesSize > 0) {
        memcpy((void*)mIndices, (void*)indices, indicesSize*sizeof(int));
    }*/
}

