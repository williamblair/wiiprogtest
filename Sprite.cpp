#include <Sprite.h>

VertexBuffer Sprite::sVertBuf;
bool Sprite::sVertBufInitted = false;
GameMath::Mat4 Sprite::sViewProjMat; // identity matrix

#define S_WIDTH 640
#define S_HEIGHT 480

// screen pixels to OpenGL NDC coordinates
static inline float xPixelToNDCCoord(int pxVal)
{
    return -1.0f + (2.0f * float(pxVal) / S_WIDTH);
}
static inline float yPixelToNDCCoord(int pxVal)
{
    return -1.0f + (2.0f * (S_HEIGHT - float(pxVal)) / float(S_HEIGHT));
}
static inline float pixWidthToNDCCoord(int pxVal)
{
    return 2.0f * float(pxVal) / S_WIDTH;
}
static inline float pixHeightToNDCCoord(int pxVal)
{
    return 2.0f * float(pxVal) / S_HEIGHT;
}

void Sprite::InitVertBuf()
{
    // for drawing from the top left
    float vertices[6*5] = {
        // position         texcoord
        0.0f, -1.0f, -1.0f,  0.0f, 0.0f, // bottom left
        1.0f, -1.0f, -1.0f,  1.0f, 0.0f, // bottom right
        0.0f, 0.0f, -1.0f,   0.0f, 1.0f, // top left
        
        1.0f, -1.0f, -1.0f,  1.0f, 0.0f, // bottom right
        1.0f, 0.0f, -1.0f,   1.0f, 1.0f, // top right
        0.0f, 0.0f, -1.0f,   0.0f, 1.0f // top left
    };
    sVertBuf.Init(
        vertices,
        6*5,
        nullptr,
        0,
        5 // floats per vertex
    );
    
    sVertBufInitted = true;
}

Sprite::Sprite() :
    mTexture(nullptr),
    mX(0),
    mY(0)
{}

Sprite::~Sprite()
{}

void Sprite::Init(Texture* tex)
{
    mTexture = tex;
    float scaleX = pixWidthToNDCCoord(tex->GetWidth());
    float scaleY = pixHeightToNDCCoord(tex->GetHeight());
    float posX = xPixelToNDCCoord(mX);
    float posY = yPixelToNDCCoord(mY);
    mScaleMat = GameMath::Scale(scaleX, scaleY, 1.0f);
    mTransMat = GameMath::Translate(posX, posY, 0.0f);
    
    if (!sVertBufInitted) {
        InitVertBuf();
    }
}

void Sprite::Draw(Renderer& render)
{
    GameMath::Mat4 modelMat = mTransMat * mScaleMat;
    render.SetTexture(*mTexture);
    render.DisableDepthTest();
    // viewmat == projmat == identity
    render.DrawVertexBuffer(modelMat, sViewProjMat, sVertBuf, &sViewProjMat);
    render.EnableDepthTest();
}

void Sprite::SetTexture(Texture* tex)
{
    mTexture = tex;
}

void Sprite::SetDrawSize(int width, int height)
{
    float scaleX = pixWidthToNDCCoord(width);
    float scaleY = pixHeightToNDCCoord(height);
    mScaleMat = GameMath::Scale(scaleX, scaleY, 1.0f);
}

void Sprite::SetPosition(int x, int y)
{
    mX = x;
    mY = y;
    float posX = xPixelToNDCCoord(mX);
    float posY = yPixelToNDCCoord(mY);
    mTransMat = GameMath::Translate(posX, posY, 0.0f);
}

