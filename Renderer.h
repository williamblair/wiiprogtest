#ifndef RENDERER_H_INCLUDED
#define RENDERER_H_INCLUDED

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include <gccore.h>
#include <wiiuse/wpad.h>
#include <fat.h>

#include <VertexBuffer.h>
#include <Texture.h>
#include <AABB.h>
#include <GameMath/wii/GameMath.h>

#define MTXCAST(m) ((f32(*)[4])(m).v)

class Renderer
{
public:
    Renderer();
    ~Renderer();
    
    bool Init(int width, int height, const char* title);
    void SetBackgroundColor(unsigned char r, unsigned char g, unsigned char b) {
        mBgColor.r = r;
        mBgColor.g = g;
        mBgColor.b = b;
        // clears the bg to color and clears the zbuffer
        GX_SetCopyClear(mBgColor, 0x00FFFFFF);
    }
    void SetTexture(Texture& t) {
        //GX_InvalidateTexAll();
        GX_LoadTexObj(&t.mTexture, GX_TEXMAP0);
    }
    void DisableDepthTest() { GX_SetZMode(GX_FALSE, 0,0); }
    void EnableDepthTest() { GX_SetZMode(GX_TRUE, GX_LEQUAL, GX_TRUE); }
    
    void Clear() {
        
        // do this before drawing
        GX_SetViewport(0, 0, mRMode->fbWidth, mRMode->efbHeight, 0, 1);
        
        GX_SetTevOp(GX_TEVSTAGE0, GX_REPLACE);
        GX_SetTevOrder(GX_TEVSTAGE0, GX_TEXCOORD0, GX_TEXMAP0, GX_COLOR0A0);
    }
    
    // TODO - reorganize
    bool DrawVertexBuffer(GameMath::Mat4& modelMat, GameMath::Mat4& viewMat, VertexBuffer& vb, GameMath::Mat4* projMat = nullptr)
    {
        GameMath::Mat4 modelview;
        modelview = viewMat * modelMat;

        // setup vertex descriptor
        // tells the flipper to expect direct data
        GX_ClearVtxDesc();
        GX_SetVtxDesc(GX_VA_POS, GX_DIRECT);
        //GX_SetVtxDesc(GX_VA_CLR0, GX_DIRECT);
        GX_SetVtxDesc(GX_VA_TEX0, GX_DIRECT);
        
        if (projMat == nullptr) {
            GX_LoadProjectionMtx((f32(*)[4])mPerspMat.v, GX_PERSPECTIVE);
        }
        else {
            // TODO - assumes this is for 2d sprite drawing
            GX_LoadProjectionMtx((f32(*)[4])projMat->v, GX_PERSPECTIVE);
        }
        
        // load the modelview matrix into matrix memory
        GX_LoadPosMtxImm(MTXCAST(modelview), GX_PNMTX0);
        //GX_LoadPosMtxImm(view, GX_PNMTX0);
        
        // position only, non indexed
        if (vb.mFloatsPerVertex == 3 && vb.mIndicesSize == 0)
        {        
            int vertexCount = vb.mVerticesSize / vb.mFloatsPerVertex;
            GX_Begin(GX_TRIANGLES, GX_VTXFMT0, vertexCount);
                for (size_t i = 0; i < vb.mVerticesSize; i += vb.mFloatsPerVertex)
                {
                    GX_Position3f32(vb.mVertices[i+0], vb.mVertices[i+1], vb.mVertices[i+2]);
                }
            GX_End();
            return true;
        }
        // position only, indexed
        if (vb.mFloatsPerVertex == 3 && vb.mIndicesSize > 0)
        {
            int vertexCount = vb.mIndicesSize;
            GX_Begin(GX_TRIANGLES, GX_VTXFMT0, vertexCount);
                for (size_t i = 0; i < vb.mIndicesSize; ++i)
                {
                    float* vert = &vb.mVertices[vb.mIndices[i]*3];
                    GX_Position3f32(vert[0], vert[1], vert[2]);
                }
            GX_End();
            return true;
        }
        // position and tex coord, non indexed
        if (vb.mFloatsPerVertex == 5 && vb.mIndicesSize == 0)
        {
            int vertexCount = vb.mVerticesSize / vb.mFloatsPerVertex;
            GX_Begin(GX_TRIANGLES, GX_VTXFMT0, vertexCount);
                for (size_t i = 0; i < vb.mVerticesSize; i += vb.mFloatsPerVertex)
                {
                    GX_Position3f32(vb.mVertices[i+0], vb.mVertices[i+1], vb.mVertices[i+2]);
                    GX_TexCoord2f32(vb.mVertices[i+3], vb.mVertices[i+4]);
                }
            GX_End();
            return true;
        }
        // position and tex coord, indexed
        if (vb.mFloatsPerVertex == 5 && vb.mIndicesSize > 0)
        {
            int vertexCount = vb.mIndicesSize;
            GX_Begin(GX_TRIANGLES, GX_VTXFMT0, vertexCount);
                for (size_t i = 0; i < vb.mIndicesSize; ++i)
                {
                    float* vert = &vb.mVertices[vb.mIndices[i]*5];
                    GX_Position3f32(vert[0], vert[1], vert[2]);
                    GX_TexCoord2f32(vert[3], vert[4]);
                }
            GX_End();
            return true;
        }
        printf("Unhandled floats per vertex: %u\n", vb.mFloatsPerVertex);
        return false;
    }

    void DrawAabb(const GameMath::Mat4& viewMat, const AABB& aabb);
    
    void Update();

    GameMath::Mat4& GetProjMat() { return mPerspMat; }
    int GetScreenWidth() const { return mRMode->fbWidth; }
    int GetScreenHeight() const { return mRMode->efbHeight; }

private:
    
    u32 mFbIndex = 0;
    void* mFrameBuffer[2] = { NULL, NULL };
    GXRModeObj* mRMode;
    
    GameMath::Mat4 mPerspMat;
    
    GXColor mBgColor = { 0, 0, 0, 0xff };
    
    f32 yscale;
    u32 xfbHeight;
    
    GXTexObj texture;
    TPLFile neheTPL;
};

#endif // RENDERER_H_INCLUDED

