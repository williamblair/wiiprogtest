#ifndef SPRITE_H_INCLUDED
#define SPRITE_H_INCLUDED

#include <GameMath/wii/GameMath.h>
#include <Texture.h>
#include <VertexBuffer.h>
#include <Renderer.h>

class Sprite
{
friend class Renderer;
    
public:

    Sprite();
    ~Sprite();
    
    void Init(Texture* tex);
    void Draw(Renderer& render);
    
    void SetTexture(Texture* tex);
    void SetDrawSize(int width, int height);
    void SetPosition(int x, int y);

private:
    Texture* mTexture;
    int mX, mY;
    GameMath::Mat4 mScaleMat;
    GameMath::Mat4 mTransMat;
    
    static VertexBuffer sVertBuf;
    static bool sVertBufInitted;
    static GameMath::Mat4 sViewProjMat;
    
    static void InitVertBuf();
};

#endif // SPRITE_H_INCLUDED
