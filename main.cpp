#include <stdio.h>

#include <Renderer.h>
#include <Camera.h>
#include <VertexBuffer.h>
#include <Texture.h>
#include <Input.h>
#include <IqmMesh.h>
#include <GameTimer.h>
#include <TileFloor.h>
#include <ThirdPersonPlayer.h>
#include <Music.h>
#include <Sprite.h>

// IQM texture
#include "Gina.h"
#include "Gina_tpl.h"
#include "zombie.h"
#include "zombie_tpl.h"

// Ground texture
#include "grass.h"
#include "grass_tpl.h"

// cross-hair texture
#include "crosshair.h"
#include "crosshair_tpl.h"
#include "crosshairred.h"
#include "crosshairred_tpl.h"
// crosshair draw size
#define CROSSHAIR_WIDTH 64
#define CROSSHAIR_HEIGHT 64

// OGG sound file
#include "sample_ogg.h"

static Renderer gRender;
//static FPSCamera gCamera;
static Input gInput;
static GameTimer gTimer;
static ThirdPersonPlayer gPlayer;

static Texture gFloorTex;
static TileFloor gFloor;

static Sprite gPointerSpr;
static Texture gPointerTex;
static Texture gPointerTexRed;

static Music gMusic;

namespace Gina
{
static IqmMesh iqm;
static Texture iqmTex;

static bool Init()
{
    if (!iqm.Init("sd:/gina.iqm")) { return false; }
    iqmTex.Init(Gina_tpl, Gina_tpl_size, gina);
    
    IqmMesh::Anim idleAnim = { 0, 180, "idle" };
    IqmMesh::Anim walkAnim = { 181, 181+30-1, "walk" };
    iqm.AddAnim(idleAnim);
    iqm.AddAnim(walkAnim);
    iqm.SetAnim("idle");
    return true;
}
} // end namespace Gina

class Zombie
{
public:
    
    Zombie() :
        mYaw(180.0f),
        mDrawPitchOffs(-90.0f)
    {}
    ~Zombie() {}
    
    bool Init()
    {
        if (!sModelLoaded) {
            if (!sIqm.Init("sd:/zombie.iqm")) { return false; }
            sTex.Init(zombie_tpl, zombie_tpl_size, zombie);
            IqmMesh::Anim idleAnim = { 0, 96, "walk" };
            IqmMesh::Anim walkAnim = { 97, 97+134-1, "idle" };
            sIqm.AddAnim(idleAnim);
            sIqm.AddAnim(walkAnim);
            sIqm.SetAnim("walk");
            sModelLoaded = true;
        }
        
        UpdateModelMat();

        return true;
    }
    
    void Update(const float dt)
    {
        sIqm.Update(dt * 24.0f); // 30 FPS
    }
    
    void SetPosition(const GameMath::Vec3& pos) { mPosition = pos; UpdateModelMat(); }
    void SetScale(const GameMath::Vec3& scale) { mModelScale = scale; UpdateModelMat(); }
    
    void Draw(GameMath::Mat4& viewMat, Renderer& render)
    {
        render.SetTexture(sTex);
        sIqm.DrawVertexBuffer(mModelMat, viewMat, render, 0);
    }
    
private:
    static IqmMesh sIqm;
    static Texture sTex;
    static bool sModelLoaded;
    
    GameMath::Mat4 mModelMat;
    GameMath::Vec3 mPosition;
    GameMath::Vec3 mModelScale;
    AABB mAabb;
    MoveComponent mMoveComp;
    float mYaw;
    float mDrawPitchOffs;
    
    void UpdateModelMat()
    {        
        mModelMat =
        GameMath::Translate(
            mMoveComp.position.x,
            mMoveComp.position.y,
            mMoveComp.position.z
        ) *
        GameMath::Rotate(GameMath::Deg2Rad(mYaw),
            GameMath::Vec3(0.0f, 1.0f, 0.0f)) *
        GameMath::Rotate(GameMath::Deg2Rad(mDrawPitchOffs),
            GameMath::Vec3(1.0f, 0.0f, 0.0f)) *
        GameMath::Scale(mModelScale.x, mModelScale.y, mModelScale.z);
    }
};
IqmMesh Zombie::sIqm;
Texture Zombie::sTex;
bool Zombie::sModelLoaded = false;
Zombie gZombie;

void MovePlayer(const float dt)
{
    const float moveSpeed = 10.0f;
    bool walked = false;
    
    if (gInput.UpHeld()) { gPlayer.Move(0.0f, -1.0f, moveSpeed * dt); walked = true; }
    else if (gInput.DownHeld()) { gPlayer.Move(0.0f, 1.0f, moveSpeed * dt); walked = true;  }
    
    if (gInput.LeftHeld()) { gPlayer.Move(1.0f, 0.0f, moveSpeed * dt); walked = true;  }
    else if (gInput.RightHeld()) { gPlayer.Move(-1.0f, 0.0f, moveSpeed * dt); walked = true;  }
    
    // force player anim back to idle
    if (!walked) {
        gPlayer.Move(0.0f, 0.0f, moveSpeed * dt); // force idle anim by default
    }
}

int main(int argc, char **argv)
{
    AABB testAabb;
    
    if (!gRender.Init(640, 480, "Does Nothing")) { return 1; }
    gRender.SetBackgroundColor(0, 50, 50);
    
    if (!Gina::Init()) { return 1; }
    if (!gZombie.Init()) { return 1; }
    gZombie.SetScale(GameMath::Vec3(1.0f, 1.0f, 1.0f));
    
    gPlayer.SetModel(&Gina::iqm);
    gPlayer.SetTexture(&Gina::iqmTex);
    //gPlayer.Move(0.0f, 0.0f, 0.0f); // force update
    
    gFloorTex.Init(grass_tpl, grass_tpl_size, grass);
    //gFloorTex.Init(crosshair_tpl, crosshair_tpl_size, crosshair);
    gFloor.SetTexture(&gFloorTex);
    gFloor.SetPosition(GameMath::Vec3(0.0f, 0.0f, 0.0f));
    gFloor.SetScale(5.0f);
    
    gPointerTex.Init(crosshair_tpl, crosshair_tpl_size, crosshair);
    gPointerTexRed.Init(crosshairred_tpl, crosshairred_tpl_size, crosshairred);
    //gPointerSpr.Init(&gPointerTex);
    gPointerSpr.Init(&gPointerTex);
    gPointerSpr.SetDrawSize(CROSSHAIR_WIDTH, CROSSHAIR_HEIGHT);
    
    if (!gMusic.Init(sample_ogg, sample_ogg_size)) { return 1; }
    //gMusic.Play();
    
    gTimer.Update();
    for (;;)
    {
        const float dt = gTimer.Update();
        gInput.Update();
        MovePlayer(dt);
        gPlayer.Update(dt);
        gZombie.Update(dt);
        
        int sprX = gInput.GetWiimoteX();
        int sprY = gInput.GetWiimoteY();
        if (sprX < 0) sprX = 0;
        if (sprY < 0) sprY = 0;
        if (sprX > (640 - CROSSHAIR_WIDTH)) sprX = 640 - CROSSHAIR_WIDTH;
        if (sprY > (480 - CROSSHAIR_HEIGHT)) sprY = 480 - CROSSHAIR_HEIGHT;
        gPointerSpr.SetPosition(
            sprX,
            sprY
        );
        
        if (gInput.ConfirmClicked()) { printf("Input confirm clicked!\n"); }
        
        AABB& plyrAabb = gPlayer.GetAabb();
        GameMath::Vec3 minWorld = plyrAabb.pos + plyrAabb.min;
        GameMath::Vec3 maxWorld = plyrAabb.pos + plyrAabb.max;

        // test world to screen pos
        GameMath::Vec2 plyrScreenMin = GameMath::WorldToScreenCoords(
            minWorld,
            gPlayer.GetViewMat(),
            gRender.GetProjMat(),
            gRender.GetScreenWidth(),
            gRender.GetScreenHeight()
        );
        GameMath::Vec2 plyrScreenMax = GameMath::WorldToScreenCoords(
            maxWorld,
            gPlayer.GetViewMat(),
            gRender.GetProjMat(),
            gRender.GetScreenWidth(),
            gRender.GetScreenHeight()
        );
        // sprX is left side of sprite
        if (sprX + (CROSSHAIR_WIDTH/2) > plyrScreenMin.x &&
            sprX + (CROSSHAIR_WIDTH/2) < plyrScreenMax.x &&
            sprY + (CROSSHAIR_HEIGHT/2) > plyrScreenMax.y && // y is flipped
            sprY + (CROSSHAIR_HEIGHT/2) < plyrScreenMin.y)
        {
            gPointerSpr.SetTexture(&gPointerTexRed);
        }
        else
        {
            gPointerSpr.SetTexture(&gPointerTex);
        }
        
        gRender.Clear();
        if (!gFloor.Draw(gPlayer.GetViewMat(), gRender)) { return 1; }
        gPlayer.Draw(gRender);
        gZombie.Draw(gPlayer.GetViewMat(), gRender);
        
        gPointerSpr.Draw(gRender);
        
        gRender.Update();
    }
    
    return 0;
}

