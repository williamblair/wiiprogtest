#ifndef MUSIC_H_INCLUDED
#define MUSIC_H_INCLUDED

class Music
{
public:

    Music();
    ~Music();

    // Init from memory; buffer must remain valid while playing (no copying done)
    // Memory must be of an OGG file
    bool Init(const void* buffer, int len);
    
    void Play(bool loop = true);
    void Stop();
    void Pause();
    void Resume();

private:
    void* mBuffer;
    int mBufferLen;
};

#endif // MUSIC_H_INCLUDED
