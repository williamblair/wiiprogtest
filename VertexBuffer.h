#ifndef WII_VERTEX_BUFFER_H_INCLUDED
#define WII_VERTEX_BUFFER_H_INCLUDED

#include <cstddef>

class VertexBuffer
{

friend class Renderer;

public:
    
    VertexBuffer();
    ~VertexBuffer();
    
    void Init(
        float* vertices,
        size_t verticesSize,
        int* indices,
        size_t indicesSize,
        size_t floatsPerVertex
    );
    
    void UpdateVertices(
        float* vertices,
        size_t verticesSize/*,
        int* indices,
        size_t indicesSize,
        size_t floatsPerVertex*/
    );

private:
    float* mVertices;
    size_t mVerticesSize;
    int* mIndices;
    size_t mIndicesSize;
    size_t mFloatsPerVertex;
};

#endif // WII_VERTEX_BUFFER_H_INCLUDED

