#ifndef WII_INPUT_H_INCLUDED
#define WII_INPUT_H_INCLUDED

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include <gccore.h>
#include <wiiuse/wpad.h>

class Input
{
public:
    
    Input();
    ~Input();
    
    void Update();
    
    bool ConfirmClicked() const { return mConfirmClicked; }
    
    bool UpHeld() const { return mUpHeld; }
    bool DownHeld() const { return mDownHeld; }
    bool LeftHeld() const { return mLeftHeld; }
    bool RightHeld() const { return mRightHeld; }
    
    float GetWiimoteX() const { return mWiimoteX; }
    float GetWiimoteY() const { return mWiimoteY; }
    
private:
    bool mConfirmClicked;
    bool mUpHeld;
    bool mDownHeld;
    bool mLeftHeld;
    bool mRightHeld;
    
    u32 mDown;
    
    float mWiimoteX;
    float mWiimoteY;
    
    ir_t mIr;
};

#endif // WII_INPUT_H_INCLUDED
