#include <Input.h>

Input::Input() :
    mConfirmClicked(false),
    mUpHeld(false),
    mDownHeld(false),
    mLeftHeld(false),
    mRightHeld(false),
    mDown(0),
    mWiimoteX(0.0f),
    mWiimoteY(0.0f)
{
}

Input::~Input()
{
}

void Input::Update()
{
    WPAD_ScanPads();
    mDown = WPAD_ButtonsDown(0);
    u32 held = WPAD_ButtonsHeld(0);
    
    // IR movement
    WPAD_IR(0, &mIr);
    
    mWiimoteX = mIr.x;
    mWiimoteY = mIr.y;
    
    mConfirmClicked = false;
    mUpHeld = false;
    mDownHeld = false;
    mLeftHeld = false;
    mRightHeld = false;
    if (mDown & WPAD_BUTTON_A) { mConfirmClicked = true; }
    
    if (held & WPAD_BUTTON_UP) { mUpHeld = true; }
    if (held & WPAD_BUTTON_DOWN) { mDownHeld = true; }
    if (held & WPAD_BUTTON_LEFT) { mLeftHeld = true; }
    if (held & WPAD_BUTTON_RIGHT) { mRightHeld = true; }
}

