#ifndef GAME_TIMER_H_INCLUDED
#define GAME_TIMER_H_INCLUDED

#include <sys/time.h>

class GameTimer
{
public:

    GameTimer();
    ~GameTimer();
    
    // returns delta time in seconds
    float Update();

private:
    struct timeval mLastTime = {0,0};
};

#endif // GAME_TIMER_H_INCLUDED
