#ifndef WII_TEXTURE_H_INCLUDED
#define WII_TEXTURE_H_INCLUDED

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include <gccore.h>
#include <wiiuse/wpad.h>

class Texture
{

friend class Renderer;

public:

    Texture() {}
    ~Texture() {}
    
    // Init from raw data
    void Init(const void* tplData, size_t tplSize, s32 texData)
    {
        TPL_OpenTPLFromMemory(&mTPL, (void*)tplData, tplSize);
        TPL_GetTexture(&mTPL, texData, &mTexture);
        mWidth =  (int)GX_GetTexObjWidth(&mTexture);
        mHeight = (int)GX_GetTexObjHeight(&mTexture);
    }
    
    int GetWidth() const { return mWidth; }
    int GetHeight() const { return mHeight; }
    
private:
    GXTexObj mTexture;
    TPLFile mTPL;
    int mWidth;
    int mHeight;
};

#endif // WII_TEXTURE_H_INCLUDED
